# Chipher Project

## About
This repository includes a program for encrypting text.

There are three types of encryption used here:
- The Vigenere Cipher
- Symmetric AES encryption
- Combined encryption using RSA and AES

## Set up

Clone this repository

```
git clone https://gitlab.com/Bushmester/project_cipher.git
```

## Configuration to run
Install the modules
```
pip install -r requirements.txt
```

### Only for telegram bot
To create in .env file
- BOT_TOKEN (for a bot token from @botfather)
- PUB_KEY (for the directory where the public key will be saved)
- PRI_KEY (for the directory where the private key will be saved)
- DOWNLOAD (for the directory where files will be saved)
- AES (for the directory where encryption aes files will be saved)
- COMBO (for the directory where encryption combo files will be saved)
- COMBO_DEC (for the directory where decryption aes files will be saved)


## Launching

### To run the console program 
Go to the directory

```
cd ./project_cipher/cmd_code
```
Use this command
```
python main.py
```

### To run the telegram bot
Go to the directory

```
cd ./project_cipher/telegram_bot
```
Use this command
```
python main.py
```