from aiogram.dispatcher.filters.state import StatesGroup, State

class Stor_Data(StatesGroup):
    Password = State()

    Text = State()
    Key = State() #

    Text_two = State()
    Key_two = State() #

    Text_thr = State()
    Key_thr = State() #

    Text_for = State()
    Key_for = State() #

    Text_fiv = State()
    Key_fiv = State() #

    Text_six = State()
    Key_six = State() #

    Text_sev = State() #
    Key_sev = State()

    Text_egh = State() #
    Key_egh = State()

    Text_nin = State()
    Key_nin = State()

    Text_ten = State()
    Key_ten = State()

    Text_eln = State()
    Key_eln = State()

    Text_twe = State()
    Key_twe = State()
