import os

from dotenv import load_dotenv

load_dotenv()

BOT_TOKEN = os.getenv('BOT_TOKEN')

pub_key = os.getenv('PUB_KEY')
pri_key = os.getenv('PRI_KEY')

download_file = os.getenv('DOWNLOAD')
aes_file = os.getenv('AES')
combo = os.getenv('COMBO')
combo_dec = os.getenv('COMBO_DEC')
