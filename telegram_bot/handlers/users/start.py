from aiogram import types
from aiogram.dispatcher.filters.builtin import CommandStart

from loader import dp

@dp.message_handler(CommandStart())
async def start(message: types.Message):
    await message.answer(
        f'Привет, {message.from_user.full_name}! \n'
        'Меня зовут Чефер и я бот криптор! \n'
        'Жми /help и я покажу, что умею'
    )