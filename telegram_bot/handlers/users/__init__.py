from .help import dp
from .start import dp
from .download import dp
from .gen import dp
from .encry import dp
from .decry import dp
from .encry_file import dp
from .decry_file import dp

__all__ = ['dp']
