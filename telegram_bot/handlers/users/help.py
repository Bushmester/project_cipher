from aiogram import types
from aiogram.dispatcher.filters import Command

from loader import dp


@dp.message_handler(Command('help'))
async def show_help(message: types.Message):
    await message.answer(
        '/pass - сгенерирую тебе пароль \n'
        '/key - сгенерирую тебе публичный и приватный ключ \n'
        '/encry - зашифрую твой текст \n'
        '/encry_file - зашифрую твой файл \n'
        '/decry - расшифрую твой текст \n'
        '/decry_file - расшифрую твой файл\n'
        'Для отмены нажмите /cancel'
    )
