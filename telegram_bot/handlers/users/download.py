from aiogram import types

from loader import dp
from config import download_file


@dp.message_handler(content_types='document')
async def retrieve_file(message: types.Message):
    await message.document.download(download_file)

