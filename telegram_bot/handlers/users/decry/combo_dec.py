import os, asyncio

from loader import dp, bot
from config import download_file, combo, combo_dec

from aiogram import types
from aiogram.dispatcher.filters import Command
from aiogram.dispatcher import FSMContext

from handlers.users.func.combo import combo_decryption
from state.stor_data import Stor_Data


@dp.message_handler(Command('combo_dec_f'))
async def combo_de_enter(message: types.Message):
    os.rename(download_file, combo)
    
    await message.answer('Загрузи приватный ключ')

    await asyncio.sleep(10)

    await message.answer('Введи пароль')

    await Stor_Data.Key_nin.set()

@dp.message_handler(state=Stor_Data.Key_nin)
async def combo_text(message: types.Message, state: FSMContext):
    answer1 = message.text
    await state.update_data(answer1=answer1)
    store = await state.get_data()
    daat = store.get('answer1')
    password = daat.encode()

    dec_text = combo_decryption(password, download_file, combo)

    await asyncio.sleep(5)

    user_id = message.from_user.id

    with open(combo_dec, 'rb') as f:
        await bot.send_document(chat_id=user_id, document=f)
    
    await state.finish()

    os.remove(combo)
    os.remove(combo_dec)
    os.remove(download_file)

