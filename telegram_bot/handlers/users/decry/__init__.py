from .decry import dp
from .simple_dec import dp
from .aes_dec import dp
from .combo_dec import dp


__all__ = ['dp']
