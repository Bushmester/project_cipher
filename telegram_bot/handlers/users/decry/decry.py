from aiogram import types
from aiogram.dispatcher.filters import Command

from loader import dp


@dp.message_handler(Command('decry'))
async def decry_v(message: types.Message):
    await message.answer(
        '/simple_dec - шифр Вижинера \n' 
        '\n'
        '/aes_dec_f - симметричное шифрование (ДЛЯ НАЧАЛА ЗАГРУЗИ ФАЙЛ) \n'
        '\n'
        '/combo_dec_f - комбинированное шифрование (ДЛЯ НАЧАЛА ЗАГРУЗИ ФАЙЛ) \n'
        '\n'
        'Для /simple_dec и /aes_dec нужны только пароли \n'
        'Для /combo_dec нужны два ключа (публичный и приватный)\n'
        'Для отмены нажмите /cancel'
    )
