def viginer_encryption(data, key, answer = ''): # Функция шифрования
    for i, j in enumerate(data):
        if j != ' ':
            sum_data = ord(j) + ord(key[i])
            answer += chr(sum_data % 1110)
        else:
            answer += ' '
    return answer

def viginer_decryption(data, key, answer = ''): # Функция расшифрования
    for i, j in enumerate(data):
        if j != ' ':
            sum_data = ord(j) - ord(key[i])
            answer += chr(sum_data % 1110)
        else:
            answer += ' '       
    return answer
