from Crypto.Cipher import AES
from Crypto.Hash import SHA256

def aes_encryption(data, password, plug = b'0'):
    password = password.encode()

    cipher = AES.new(
        key=SHA256.new(password).digest(), 
        mode=AES.MODE_CBC,
        iv='This is an IV456'.encode("utf8")
    )
    
    while len(data) % 16 != 0:
        data += plug

    encrypt = cipher.encrypt(data)

    return encrypt


def aes_decryption(data, password, plug = b'0'):
    password = password.encode()
    
    cipher = AES.new(
        key=SHA256.new(password).digest(), 
        mode=AES.MODE_CBC,
        iv='This is an IV456'.encode("utf8")
    )

    while len(data) % 16 != 0:
        data += plug    
    
    decrypt = cipher.decrypt(data)

    return decrypt.rstrip(plug)
