from loader import pub_key, pri_key

from Crypto.PublicKey import RSA

def gen_priv_and_pub_rsa_key(password):
    key = RSA.generate(2048)
    
    private_key = key.exportKey(
        format='PEM',
        passphrase=password,
        pkcs=8, 
        protection="scryptAndAES256-CBC"
    )

    with open(pri_key, 'wb') as f:
        f.write(private_key)


    public_key = key.publickey().exportKey()

    with open(pub_key, 'wb') as f:
        f.write(public_key)
