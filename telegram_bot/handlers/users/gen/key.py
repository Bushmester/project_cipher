import os

from aiogram import types
from aiogram.dispatcher.filters import Command, Text
from aiogram.dispatcher import FSMContext

from loader import bot, dp, pri_key, pub_key

from .gen_key_for_rsa import gen_priv_and_pub_rsa_key
from state.stor_data import Stor_Data


@dp.message_handler(Command('key'))
async def enter_pass(message: types.Message):
    await message.answer(
        'Придумай и введи пароль. Можешь использовать /pass'
    )

    await Stor_Data.Password.set()


@dp.message_handler(state=Stor_Data.Password)
async def send_key(message: types.Message, state: FSMContext):
    answer = message.text
    await state.update_data(answer=answer)
    store = await state.get_data()

    password = store['answer']
    password = password.encode()
    user_id = message.from_user.id
    gen = gen_priv_and_pub_rsa_key(password=password)
    
    with open(pri_key, 'rb') as f:
        await bot.send_document(chat_id=user_id, document=f)

    with open(pub_key, 'rb') as f:
        await bot.send_document(chat_id=user_id, document=f)

    await state.finish()

    os.remove(pri_key)
    os.remove(pub_key)
