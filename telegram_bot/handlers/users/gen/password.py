import random

from aiogram import types
from aiogram.dispatcher.filters import Command

from loader import dp


@dp.message_handler(Command('pass'))
async def gen_password(message: types.Message):
    symbols = [chr(x) for x in range(ord('!'), ord('~') + 1)]
    len_pass = [int(x) for x in range(5, 11)]
    len_pass = random.choice(len_pass)
    password = ''

    for i in range(len_pass):
        password += random.choice(symbols)
    
    answer = 'Твой сгенерированный пароль: ' + password

    await message.answer(answer)
