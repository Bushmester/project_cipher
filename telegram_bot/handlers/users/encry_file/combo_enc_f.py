import os, asyncio

from loader import dp, bot
from config import download_file, combo

from aiogram import types
from aiogram.dispatcher.filters import Command

from handlers.users.func.combo import combo_encryption

@dp.message_handler(Command('combo_file'))
async def combo_enter(message: types.Message):
    with open(download_file, 'rb') as f:
        text = f.read()
    
    await message.answer('Загрузи публичный ключ')

    await asyncio.sleep(10)

    enc_text = combo_encryption(text, pub_f=download_file)

    user_id = message.from_user.id

    with open(combo, 'rb') as f:
        await bot.send_document(chat_id=user_id, document=f)
    
    os.getenv(combo)
    os.getenv(download_file)
