import os

from loader import dp, bot
from config import download_file

from aiogram import types
from aiogram.dispatcher.filters import Command
from aiogram.dispatcher import FSMContext

from handlers.users.func.aes import aes_encryption
from state.stor_data import Stor_Data

@dp.message_handler(Command('aes_file'))
async def aes_enter(message: types.Message):
    await message.answer('Введи пароль')

    await Stor_Data.Key_sev.set()

@dp.message_handler(state=Stor_Data.Key_sev)
async def answer_text(message: types.Message, state: FSMContext):
    answer1 = message.text
    await state.update_data(answer1=answer1)
    store = await state.get_data()
    password = store.get('answer1')
    
    with open(download_file, 'rb') as f:
        text = f.read()

    dec_aes = aes_encryption(text, password)

    with open(download_file, 'wb') as f:
        decry = f.write(dec_aes)
    
    user_id = message.from_user.id
        
    with open(download_file, 'rb') as f:
        await bot.send_document(chat_id=user_id, document=f)

    await state.finish()

    os.remove(download_file)
