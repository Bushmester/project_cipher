from aiogram import types
from aiogram.dispatcher.filters import Command

from loader import dp


@dp.message_handler(Command('encry_file'))
async def encry_file(message: types.Message):
    await message.answer(
        'ЗАГРУЗИ ФАЙЛ и выбири пункт \n'
        '\n'
        '/simple_file - шифр Вижинера \n' 
        'Уровень защиты: 1 \n'
        '\n'
        '/aes_file - симметричное шифрование \n'
        'Уровень защиты: 2 \n'
        '\n'
        '/combo_file - комбинированное шифрование \n'
        'Уровень защиты: 3 \n'
        '\n'
        'Для /simple и /aes нужны только пароли\n'
        'Для /combo нужны два ключа (публичный и приватный)\n'
        'Для отмены нажмите /cancel'
    )
