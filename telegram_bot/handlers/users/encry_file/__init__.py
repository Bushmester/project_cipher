from .encry_file import dp
from .simple_enc_f import dp
from .aes_enc_f import dp
from .combo_enc_f import dp


__all__ = ['dp']
