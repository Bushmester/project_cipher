import os

from loader import dp, bot
from config import download_file

from aiogram import types
from aiogram.dispatcher.filters import Command
from aiogram.dispatcher import FSMContext

from handlers.users.func.vignere import viginer_encryption
from state.stor_data import Stor_Data

@dp.message_handler(Command('simple_file'))
async def vf_enter_pass(message: types.Message):
    await message.answer('Придумай и введи пароль')

    await Stor_Data.Key_thr.set()

@dp.message_handler(state=Stor_Data.Key_thr)
async def answer_f_pass(message: types.Message, state: FSMContext):
    answer1 = message.text
    await state.update_data(answer1=answer1)
    store = await state.get_data()
    password = store.get('answer1')

    with open(download_file, 'r') as f:
        text = f.read()
    
    password *= len(text) // len(password) + 1

    enc_text = viginer_encryption(text, password)
    
    with open(download_file, 'w') as f:
        decry = f.write(enc_text)

    user_id = message.from_user.id

    with open(download_file, 'rb') as f:
        await bot.send_document(chat_id=user_id, document=f)

    await state.finish()

    os.remove(download_file)
 