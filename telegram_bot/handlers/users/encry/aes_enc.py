import os

from loader import dp, bot
from config import aes_file

from aiogram import types
from aiogram.dispatcher.filters import Command
from aiogram.dispatcher import FSMContext

from handlers.users.func.aes import aes_encryption
from state.stor_data import Stor_Data


@dp.message_handler(Command('aes'))
async def aes_enter(message: types.Message):
    await message.answer('Введи текст')

    await Stor_Data.Text_fiv.set()

@dp.message_handler(state=Stor_Data.Text_fiv)
async def answer_aes_text(message: types.Message, state: FSMContext):
    answer1 = message.text
    await state.update_data(answer1=answer1)
    store = await state.get_data()
    text = store.get('answer1')
    
    await message.answer('Придумай и введи пароль')

    await Stor_Data.next()

@dp.message_handler(state=Stor_Data.Key_fiv)
async def answer_aes_pass(message: types.Message, state: FSMContext):
    answer2 = message.text
    await state.update_data(answer2=answer2)
    store = await state.get_data()
    key = store.get('answer2')

    text = store['answer1'].encode()
    key = store['answer2']

    enc_text = aes_encryption(text, key)

    with open(aes_file, 'wb') as f:
        enc_a = f.write(enc_text)

    user_id = message.from_user.id
        
    with open(aes_file, 'rb') as f:
        await bot.send_document(chat_id=user_id, document=f)

    await state.finish()

    os.remove(aes_file)
