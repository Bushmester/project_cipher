from aiogram import types
from aiogram.dispatcher.filters import Command

from loader import dp


@dp.message_handler(Command('encry'))
async def encry_v(message: types.Message):
    await message.answer(
        '/simple - шифр Вижинера \n' 
        'Уровень защиты: 1 \n'
        '\n'
        '/aes - симметричное шифрование \n'
        'Уровень защиты: 2 \n'
        '\n'
        '/combo - комбинированное шифрование (ДЛЯ НАЧАЛА ОТПРАВИТЬ ПУБЛИЧНЫЙ КЛЮЧ)\n'
        'Уровень защиты: 3 \n'
        '\n'
        'Для /simple и /aes нужны только пароли\n'
        'Для /combo нужны два ключа (публичный и приватный)\n'
        'Для отмены нажмите /cancel'
    )
