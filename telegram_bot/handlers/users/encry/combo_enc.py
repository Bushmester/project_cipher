import os

from loader import dp, bot
from config import download_file, combo

from aiogram import types
from aiogram.dispatcher.filters import Command
from aiogram.dispatcher import FSMContext

from handlers.users.func.combo import combo_encryption
from state.stor_data import Stor_Data

@dp.message_handler(Command('combo'))
async def combo_enter(message: types.Message):
    await message.answer('Введи текст')

    await Stor_Data.Text_egh.set()

@dp.message_handler(state=Stor_Data.Text_egh)
async def combo_text(message: types.Message, state: FSMContext):
    answer1 = message.text
    await state.update_data(answer1=answer1)
    store = await state.get_data()
    daat = store.get('answer1')
    text = daat.encode()
    enc_text = combo_encryption(text, download_file)

    user_id = message.from_user.id

    with open(combo, 'rb') as f:
        await bot.send_document(chat_id=user_id, document=f)
    
    await state.finish()

    os.getenv(combo)
