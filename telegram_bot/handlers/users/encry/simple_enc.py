from aiogram import types
from aiogram.dispatcher.filters import Command
from aiogram.dispatcher import FSMContext

from loader import dp

from handlers.users.func.vignere import viginer_encryption
from state.stor_data import Stor_Data


@dp.message_handler(Command('simple'))
async def v_enter(message: types.Message):
    await message.answer('Введи текст')

    await Stor_Data.Text.set()

@dp.message_handler(state=Stor_Data.Text)
async def answer_text(message: types.Message, state: FSMContext):
    answer1 = message.text
    await state.update_data(answer1=answer1)
    store = await state.get_data()
    text = store.get('answer1')
    
    await message.answer('Придумай и введи пароль')

    await Stor_Data.next()

@dp.message_handler(state=Stor_Data.Key)
async def answer_pass(message: types.Message, state: FSMContext):
    answer2 = message.text
    await state.update_data(answer2=answer2)
    store = await state.get_data()
    key = store.get('answer2')

    text = store['answer1']
    key = store['answer2']

    key *= len(text) // len(key) + 1
    print(text, key)

    enc_text = viginer_encryption(text, key)
    await message.answer(enc_text)

    await state.finish()
