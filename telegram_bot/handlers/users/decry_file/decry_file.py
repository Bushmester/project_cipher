from aiogram import types
from aiogram.dispatcher.filters import Command

from loader import dp


@dp.message_handler(Command('decry_file'))
async def decry_vf(message: types.Message):
    await message.answer(
        'ЗАГРУЗИ ФАЙЛ и выбири пункт \n'
        '\n'
        '/simple_dec_f - шифр Вижинера \n' 
        '\n'
        '/aes_dec_f - симметричное шифрование \n'
        '\n'
        '/combo_dec_f - комбинированное шифрование \n'
        '\n'
        'Для /simple_dec_f и /aes_dec_f нужны только пароли \n'
        'Для /combo_dec_f нужны два ключа (публичный и приватный)\n'
        'Для отмены нажмите /cancel'
    )
