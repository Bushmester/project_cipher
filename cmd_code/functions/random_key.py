import random

def random_key():
    symbols = [chr(x) for x in range(ord('!'), ord('~') + 1)]
    key = ''

    for i in range(5):
        key += random.choice(symbols)

    return key
