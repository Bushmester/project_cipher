from functions.read_file import write_file
from Crypto.PublicKey import RSA

def gen_priv_and_pub_rsa_key(password):
    key = RSA.generate(2048)
    
    private_key = key.exportKey(
        format='PEM',
        passphrase=password,
        pkcs=8, 
        protection="scryptAndAES256-CBC"
    )

    write_file('private_rsa_key.bin', private_key)

    public_key = key.publickey().exportKey()

    write_file('public_rsa_key.pem', public_key)
