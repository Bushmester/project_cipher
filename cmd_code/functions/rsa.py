from Crypto.PublicKey import RSA
from Crypto.Random import get_random_bytes
from Crypto.Cipher import AES, PKCS1_OAEP

def combo_encryption(data, pub_f='public_rsa_key.pem'):
    with open('encryption_file.bin', 'wb') as f:
        pub_key = RSA.import_key(
            open(pub_f).read()
        )

        session_key = get_random_bytes(16)

        enc_pub_key = PKCS1_OAEP.new(pub_key)
        enc_session_key = enc_pub_key.encrypt(session_key)
        f.write(enc_session_key)

        aes_enc_ses_key = AES.new(session_key, AES.MODE_EAX)

        encrypt_text, tag = aes_enc_ses_key.encrypt_and_digest(data)

        f.write(aes_enc_ses_key.nonce)
        f.write(tag)
        f.write(encrypt_text)


def combo_decryption(password, priv_f='private_rsa_key.bin', enc_f = 'encryption_file.bin'):
    with open(enc_f, 'rb') as f:
        priv_key = RSA.import_key(
            open(priv_f).read(),
            passphrase=password
        )

        enc_session_key, nonce, tag, encrypt_text = [
            f.read(x) for x in (priv_key.size_in_bytes(), 16, 16, -1)
        ]

        enc_priv_key = PKCS1_OAEP.new(priv_key)
        session_key = enc_priv_key.decrypt(enc_session_key)
        
        aes_enc_ses_key = AES.new(session_key, AES.MODE_EAX, nonce)
        data = aes_enc_ses_key.decrypt_and_verify(encrypt_text, tag)

    with open('decryption_file.bin', 'wb') as f:
        f.write(data)
