import getpass

from functions.read_file import read_file, write_file
from functions.random_key import random_key
from functions.simple_vignere import viginer_encryption, viginer_decryption
from functions.aes import aes_encryption, aes_decryption
from functions.gen_key_for_rsa import gen_priv_and_pub_rsa_key
from functions.rsa import combo_encryption, combo_decryption

print()
print('1) Зашифровать содержимое файла')
print('2) Расшифровать содержимое файла')
print('3) Зашифровать текст')
print('4) Расшифровать текст')
print()
try:
    answer = int(input('Выберите пункт: '))

    if answer == 1:
        print()
        print('1) Зашифровать содержимое файла шифром Виженера с собственный ключём')
        print('2) Зашифровать содержимое файла шифром Виженера сгенерированным ключём в пять символов')
        print('3) Зашифровать содержимое файла AES шифрованием')
        print('4) Зашифровать содержимое файла комбинированием AES и RES шифрованием')
        print()

        check = int(input('Выберите пункт: '))
        
        if check == 1:
            name_file = input('Введите директорию файла: ')
            key = getpass.getpass('Введите ключ: ')
            text_file = read_file(name_file, 'r')
            key *= len(text_file) // len(key) + 1
            encrypt_text = viginer_encryption(text_file, key)
            write_file = write_file(name_file, encrypt_text, 'w')
            print('Содержимое файла зашифровано')
        elif check == 2:
            name_file = input('Введите директорию файла: ')
            key = random_key()
            print('Ваш ключ: '+key)
            text_file = read_file(name_file, 'r')
            key *= len(text_file) // len(key) + 1
            encrypt_text = viginer_encryption(text_file, key)
            write_file = write_file(name_file, encrypt_text, 'w')
            print('Содержимое файла зашифровано')
        elif check == 3:
            name_file = input('Введите директорию файла: ')
            password = getpass.getpass('Введите пароль: ')
            text_file = read_file(name_file)
            encrypt_text = aes_encryption(text_file, password)
            write_file = write_file(name_file, encrypt_text)
            print('Содержимое файла зашифровано')
        elif check == 4:
            print()
            print('1) Сгенерировать ключи и зашифровать')
            print('2) Зашифровать со своими ключами')
            print()

            sec_check = int(input('Выберите пункт: '))
            
            if sec_check == 1:
                password = input('Введите пароль: ')
                generation = gen_priv_and_pub_rsa_key(password)
                print('Ключи сгенерированы')
                name_file = input('Введите дерикторию файла: ')
                text = read_file(name_file)
                encrypt_text = combo_encryption(text)
                print('Содержимое файла зашифровано')
            elif sec_check == 2:
                pub_file = input('Введите дерикторию публичного ключа: ')
                name_file = input('Введите дерикторию файла: ')
                text = read_file(name_file)
                encrypt_text = combo_encryption(text, pub_file)
                print('Содержимое файла зашифровано')
            else:
                print('Ошибка: Пункт не найден')
        else:
            print('Ошибка: Пункт не найден')

    elif answer == 2:
        print()
        print('1) Расшифровать содержимое файла шифром Виженера')
        print('2) Расшифровать содержимое файла AES шифрованием')
        print('3) Расшифровать содержимое файла комбинированием AES и RES шифрованием')
        print()
        
        check = int(input('Выберите пункт: '))

        if check == 1:
            name_file = input('Введите директорию файла: ')
            key = getpass.getpass('Введите ключ: ')
            text_file = read_file(name_file, 'r')
            key *= len(text_file) // len(key) + 1
            decrypt_text = viginer_decryption(text_file, key)
            write_file = write_file(name_file, decrypt_text, 'w')
            print('Содержимое текста расшифровано')
        elif check == 2:
            name_file = input('Введите директорию файла: ')
            password = getpass.getpass('Введите пароль: ')
            text_file = read_file(name_file)
            decrypt_text = aes_decryption(text_file, password)
            write_file = write_file(name_file, decrypt_text, 'wb')
            print('Содержимое текста расшифровано')            
        elif check == 3:
            priv_k = input('Введите дерикторию приватного ключа: ')
            e_file = input('Введите дерикторию зашифрованого файла: ')
            password = getpass.getpass('Введите пароль: ')
            decrypt_text = combo_decryption(password, priv_k, e_file)
            print('Содержимое текста расшифровано')
        else:
            print('Ошибка: Пункт не найден')

    elif answer == 3:
        print()
        print('1) Зашифровать текст шифром Виженера с собственный ключём')
        print('2) Зашифровать текст шифром Виженера сгенерированным ключём в пять символов')
        print('3) Зашифровать текст AES шифрованием')
        print('4) Зашифровать текст комбинированным AES и RES шифрованием')
        print()

        check = int(input('Выберите пункт: '))

        if check == 1:
            text = input('Введите текст: ')
            key = getpass.getpass('Введите ключ: ')
            key *= len(text) // len(key) + 1
            encrypt_text = viginer_encryption(text, key)
            print('Зашифрованный текст:', encrypt_text)
        elif check == 2:
            text = input('Введите текст: ')
            key = random_key()
            key *= len(text) // len(key) + 1
            encrypt_text = viginer_encryption(text, key)
            print('Ваш ключ:', key)
            print('Зашифрованный текст:', encrypt_text)
        elif check == 3:
            text = input('Введите текст: ').encode()
            password = getpass.getpass('Введите пароль: ')
            encrypt_text = aes_encryption(text, password)
            print('Зашифрованный текст:', encrypt_text)
        elif check == 4:
            print()
            print('1) Сгенерировать ключи и зашифровать')
            print('2) Зашифровать со своими ключами')
            print()

            sec_check = int(input('Выберите пункт: '))
            
            if sec_check == 1:
                password = input('Введите пароль: ')
                generation = gen_priv_and_pub_rsa_key(password)
                print('Ключи сгенерированы')
                text = input('Введите текст: ')
                text = text.encode()                           #str.encode(text, encoding='utf-8')
                encrypt_text = combo_encryption(text)
                print('Текст зашифрован', encrypt_text)
            elif sec_check == 2:
                pub_file = input('Введите дерикторию публичного ключа: ')
                text = input('Введите текст: ')
                text = text.encode()                           #str.encode(text, encoding='utf-8')
                encrypt_text = combo_encryption(text, pub_file)
                print('Текст зашифрован', encrypt_text)
        else:
            print('Ошибка: Пункт не найден')
            
    elif answer == 4:
        print()
        print('1) Расшифровать текст шифром Виженера')
        print('2) Расшифровать текст AES шифрованием') # Не работает (сделать с файлами)
        print('3) Расшифровать текст комбинированым AES и RES шифрованием')
        print()

        check = int(input('Выберите пункт: '))

        if check == 1:
            text = input('Введите текст: ')
            key = getpass.getpass('Введите ключ: ')
            key *= len(text) // len(key) + 1
            decrypt_text = viginer_decryption(text, key)
            print('Расшифрованный текст: '+decrypt_text)
        elif check == 2:
            text = input('Введите текст: ')
            write_text = write_file('text.txt', text, 'w')
            password = getpass.getpass('Введите пароль: ')
            decrypt_text = aes_decryption(text, password)
            print('Расшифрованный текст:', decrypt_text)
        elif check == 3:
            priv_k = input('Введите дерикторию приватного ключа: ')
            e_file = input('Введите дерикторию зашифрованого файла: ')
            password = getpass.getpass('Введите пароль: ')
            decrypt_text = combo_decryption(password, priv_k, e_file)
            print('Содержимое текста расшифровано')
        else:
            print('Ошибка: Пункт не найден')

    else:
        print('Число не найдено')
#except ValueError: 
#    print('Ошибка: Используйте только натуральные числа')
except FileNotFoundError:
    print('Ошибка: Файл не найден') 
